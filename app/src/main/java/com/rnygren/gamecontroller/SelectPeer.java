package com.rnygren.gamecontroller;

import android.content.Intent;
import android.net.wifi.p2p.WifiP2pDevice;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SelectPeer extends AppCompatActivity {
    ListView listView;
    String selectedAddress;
    private List<WifiP2pDevice> deviceList = new ArrayList<WifiP2pDevice>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_peer);
        listView  = (ListView) findViewById(R.id.selectPeer);

        //populate list
        deviceList = (ArrayList<WifiP2pDevice>) getIntent().getSerializableExtra("deviceList");

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent returnAddress = new Intent();
                returnAddress.putExtra("address",deviceList.get(position).deviceAddress);
                setResult(RESULT_OK,returnAddress);
                finish();
            }
        });
        List<String> deviceName = new ArrayList<String>();
        for (WifiP2pDevice device : deviceList)
        {
            deviceName.add(device.deviceName);
        }

        ArrayAdapter<String> devices = new ArrayAdapter<String>(SelectPeer.this,android.R.layout.simple_list_item_1,deviceName);
        listView.setAdapter(devices);
    }

}
