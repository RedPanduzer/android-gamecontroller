package com.rnygren.gamecontroller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.*;
import android.util.Log;
import android.widget.Toast;

import java.io.Console;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static android.net.wifi.p2p.WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION;

/**
 * Created by RAndom MC on 02/10/2016.
 */
public class GeneralBroadcastReceiver extends BroadcastReceiver{
    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private ReceiverMain mActivity;
    private WifiP2pManager.PeerListListener peerListListener;
    private ArrayList<WifiP2pDevice> peers;
    private String isConnected;


    public GeneralBroadcastReceiver(WifiP2pManager manager, WifiP2pManager.Channel channel,
                        ReceiverMain activity) {
        super();
        isConnected = "DISCONNECTED";
        this.mManager = manager;
        this.mChannel = channel;
        this.mActivity = activity;
        peers = new ArrayList<WifiP2pDevice>();
        peerListListener = new WifiP2pManager.PeerListListener(){
            public void onPeersAvailable(WifiP2pDeviceList peerList){
                // Out with the old, in with the new.
                peers.clear();
                peers.addAll(peerList.getDeviceList());

                // If an AdapterView is backed by this data, notify it
                // of the change.  For instance, if you have a ListView of available
                // peers, trigger an update.
                //((WiFiPeerListAdapter) getListAdapter()).notifyDataSetChanged();
                if (peers.size() == 0) {
                    Log.d("WifiTAG", "No devices found");
                    return;
                } else {
                    mActivity.peerListReady(peers);
                  /*  WifiP2pDevice peer = (WifiP2pDevice) peers.get(0);
                    if(peer.deviceName.equals("GT-I9105P")){
                        connectToPeer( peer);
                    }*/

                }
            }
        };
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            // Check to see if Wi-Fi is enabled and notify appropriate activity
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                // Wifi P2P is enabled
                //mActivity.setIsWifiP2pEnabled(true);
            } else {
                // Wi-Fi P2P is not enabled
            }

        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
            // request available peers from the wifi p2p manager. This is an
            // asynchronous call and the calling activity is notified with a
            // callback on PeerListListener.onPeersAvailable()
            if (mManager != null && (isConnected.equals("CONNECTED_PART1") || isConnected.equals("DISCONNECTED_PART1") || isConnected.equals("DISCONNECTED"))) {
                isConnected = "CONNECTED";
                mManager.requestPeers(mChannel, peerListListener);
            }

        } else if (WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
            // Respond to new connection or disconnections
            NetworkInfo info = (NetworkInfo) intent.getExtras().get(mManager.EXTRA_NETWORK_INFO);
            if(isConnected.equals("DISCONNECTED")){
                isConnected = info.getState().name()+"_PART1";
            } else {
                isConnected = info.getState().name();
            }
        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            // Respond to this device's wifi state changing
        }
    }
    void connectToPeer(String address){

        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = address;
        config.wps.setup = WpsInfo.PBC;

        mManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                // WiFiDirectBroadcastReceiver will notify us. Ignore for now.
            }

            @Override
            public void onFailure(int reason) {

            }
        });

    }


}
